angular.module('pravzhizn.api', [])

.factory('PrvApiManager', ['$http', '$q', '$window', 'LocalService', 'MtpApiManager', 'AppRuntimeManager',
    function ($http, $q, $window, LocalService, MtpApiManager, AppRuntimeManager) {
        return {
            setToken: function(token){
                LocalService.set('auth_token',token);
            },
            getToken: function(){
                return LocalService.get('auth_token');
            },
            parseJwt: function(token) {
                var base64Url = token.split('.')[1];
                var base64 = base64Url.replace('-', '+').replace('_', '/');
                return JSON.parse($window.atob(base64));
            },
            isAuthenticated: function () {
                var token = this.getToken();
                if(token) {
                    var params = this.parseJwt(token);
                    return Math.round(new Date().getTime() / 1000) <= params.exp;
                } else {
                    return false;
                }
            },
            login: function (credentials, cb) {
                var self = this;

                credentials.platform = 'web';

                self.invokePost('user/register', credentials).then(
                    function (result) {

                        self.setToken(result.token)

                        if(cb)
                            cb()
                    });
            },
            logout: function () {
                MtpApiManager.logOut().then(function () {
                    LocalService.unset('auth_token');
                    location.hash = '/login'
                    AppRuntimeManager.reload()
                })
            },
            invokeGet: function(action, params){
                var self = this;
                var deferred = $q.defer();

                $http.get(Config.Prv.api_url + action,{params: params }).then(
                    function(response) {
                        if(response.data.success){
                            deferred.resolve(response.data.data);
                        } else {
                            deferred.reject(response.data);
                        }
                    },
                    function(response) {
                        if(response.status == 401){
                            self.logout();
                        } else
                            deferred.reject(response);
                    }
                );

                return deferred.promise;
            },
            invokePost: function(action, data, params){
                var self = this;
                var deferred = $q.defer();

                $http.post(Config.Prv.api_url + action, data, params).then(
                    function(response) {
                        if(response.data.success){
                            if(response.data.data)
                                deferred.resolve(response.data.data);
                            else
                                deferred.resolve();
                        } else {
                            deferred.reject(response.data);
                        }
                    },
                    function(response) {
                        if(response.status == 401){
                            self.logout();
                        } else
                            deferred.reject(response);
                    }
                );

                return deferred.promise;
            },
            postAction: function(url, data, params){
                return this.invokePost(url, data, params);
            },
            getAction: function(url, params){
                return this.invokeGet(url, params);
            }
        };
    }])

    .factory('AuthInterceptor',['$q', '$injector',
        function ($q, $injector) {
            return {
                request: function (rConf) {
                    if(rConf.url.indexOf(Config.Prv.api_url) === 0) {
                        var LocalService = $injector.get('LocalService');
                        var token = LocalService.get('auth_token');

                        if (token) {
                            rConf.headers.Authorization = 'Bearer ' + token;
                        }


                        if(rConf.headers['Content-Type'] === false)
                            rConf.headers['Content-Type'] = undefined  //form boundary
                        else
                            rConf.headers['Content-Type'] = 'application/json; charset=UTF-8'

                    }
                    return rConf;
                },
                responseError: function (response) {
                    if(response.config.url.indexOf(Config.Prv.api_url) === 0) {
                        var LocalService = $injector.get('LocalService');

                        if (response.status === 401 || response.status === 403) {
                            LocalService.unset('auth_token');
                        }
                    }
                    return $q.reject(response);
                }
            }
        }])

    .factory('LocalService', [function () {
        return {
            get: function (key) {
                return localStorage.getItem(key);
            },
            set: function (key, val) {
                return localStorage.setItem(key, val);
            },
            unset: function (key) {
                return localStorage.removeItem(key);
            }
        }
    }])

  .service('PrvModalManager', function ($rootScope, $modal, $modalStack, $filter, $q, qSync, MtpApiManager, RichTextProcessor, ServerTimeManager, Storage, _) {
      function templeListModal(){
          var scope = $rootScope.$new()

          var modal = $modal.open({
              templateUrl: templateUrl('prv_temple_list_modal'),
              controller: 'TempleListModalController',
              scope: scope,
              windowClass: 'prv_md_simple_modal_window mobile_modal',
              backdrop: 'single'
          })

          return modal.result;
      }

      function templeViewModal(templeId){
          var scope = $rootScope.$new()
          scope.templeId = templeId

          var modal = $modal.open({
              templateUrl: templateUrl('prv_temple_view_modal'),
              controller: 'TempleViewModalController',
              scope: scope,
              windowClass: 'prv_md_simple_modal_window mobile_modal',
              backdrop: 'single'
          })

          return modal.result
      }

      function userTempleListModal(){
          var scope = $rootScope.$new()
          var modal = $modal.open({
              templateUrl: templateUrl('prv_user_temple_list_modal'),
              controller: 'UserTempleListModalController',
              scope: scope,
              windowClass: 'prv_md_simple_modal_window mobile_modal',
              backdrop: 'single'
          })
          return modal.result
      }


      function profileEditModal(){
          var modal =  $modal.open({
              templateUrl: templateUrl('prv_profile_edit_modal'),
              controller: 'PrvProfileEditModalController',
              windowClass: 'prv_md_simple_modal_window mobile_modal',
              backdrop: 'single'
          })
          return modal.result
      }

      function countryModal() {
          var modal = $modal.open({
              templateUrl: templateUrl('prv_country_select_modal'),
              controller: 'PrvCountrySelectModalController',
              windowClass: 'countries_modal_window mobile_modal hidden_close',
              backdrop: 'static'
          })

          return modal.result;
      }

      function cityModal(countryId){
          var scope = $rootScope.$new()
          scope.countryId = countryId

          var modal = $modal.open({
              templateUrl: templateUrl('prv_city_select_modal'),
              controller: 'PrvCitySelectModalController',
              scope: scope,
              windowClass: 'countries_modal_window mobile_modal hidden_close',
              backdrop: 'static'
          })

          return modal.result;
      }

      return {
         templeListModal: templeListModal,
         templeViewModal: templeViewModal,
         userTempleListModal: userTempleListModal,
         profileEditModal: profileEditModal,
         countryModal: countryModal,
         cityModal: cityModal
      }
  });