'use strict'

/* Directives */

angular.module('myApp.directives')

.directive('prvCountriesList', function ($window, $timeout) {
    return {
        link: link
    }

    function link ($scope, element, attrs) {
        var searchWrap = $('.countries_modal_search')[0]
        var panelWrap = $('.countries_modal_panel')[0]
        var countriesWrap = $('.countries_wrap', element)[0]

        onContentLoaded(function () {
            $(countriesWrap).nanoScroller({preventPageScrolling: true, tabIndex: -1, iOSNativeScrolling: true})
            updateSizes()
        })

        function updateSizes () {
            $(element).css({
                height: $($window).height()
                - (panelWrap && panelWrap.offsetHeight || 0)
                - (searchWrap && searchWrap.offsetHeight || 0)
                - (Config.Mobile ? 46 + 18 : 200)
            })
            $(countriesWrap).nanoScroller()
        }

        $($window).on('resize', updateSizes)
        $scope.$on('contacts_change', function () {
            onContentLoaded(updateSizes)
        })
    }
})

  .directive('prvScrollContent', function ($window, $timeout) {
      return {
          link: link
      }

      function link ($scope, element, attrs) {
          var scrollWrap = $('.prv_scroll_wrap', element)[0]

          onContentLoaded(function () {
              $(scrollWrap).nanoScroller({preventPageScrolling: true, tabIndex: -1, iOSNativeScrolling: true})
              updateSizes()
          })

          function updateSizes () {
              $(element).css({
                  height: Math.min(200, $($window).height()
                    - (Config.Mobile ? 46 + 18 : 200))
              })
              $(scrollWrap).nanoScroller()
          }

          $($window).on('resize', updateSizes)
      }
  })

  .directive('prvTempleList', function ($window, $timeout) {
      return {
          link: link
      }

      function link ($scope, element, attrs) {
          var searchWrap = $('.temple_list_filter')[0]
          var templesWrap = $('.temples_wrap', element)[0]

          onContentLoaded(function () {
              $(templesWrap).nanoScroller({preventPageScrolling: true, tabIndex: -1, iOSNativeScrolling: true})
              updateSizes()
          })

          function updateSizes () {
              $(element).css({
                  height: $($window).height() -
                  (searchWrap && searchWrap.offsetHeight || 0) -
                  (Config.Mobile ? 50 : 150)
              })
              $(templesWrap).nanoScroller()
          }

          $($window).on('resize', updateSizes)

          $scope.$on('temple_list_change', function () {
              onContentLoaded(updateSizes)
          })
      }
  })

  .directive('prvSaintList', function ($window, $timeout) {
      return {
          link: link
      }

      function link ($scope, element, attrs) {
          var searchWrap = $('.saint_list_filter')[0]
          var saintsWrap = $('.saints_wrap', element)[0]

          onContentLoaded(function () {
              $(saintsWrap).nanoScroller({preventPageScrolling: true, tabIndex: -1, iOSNativeScrolling: true})
              updateSizes()
          })

          function updateSizes () {
              $(element).css({
                  height: $($window).height() -
                  (searchWrap && searchWrap.offsetHeight || 0) -
                  (Config.Mobile ? 50 : 400)
              })
              $(saintsWrap).nanoScroller()
          }

          $($window).on('resize', updateSizes)

          $scope.$on('saint_list_change', function () {
              onContentLoaded(updateSizes)
          })
      }
  })
  .directive('customOnChange', function() {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var onChangeHandler = scope.$eval(attrs.customOnChange);
            element.bind('change', onChangeHandler);
        }
    };
});
