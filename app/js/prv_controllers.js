'use strict'

/**
 * Pravzhizn Controllers
 */

angular.module('myApp.controllers')
  //alex
  .controller('TempleListModalController', function ($scope, $modalInstance, $modal, $rootScope, MtpApiManager, AppUsersManager, AppChatsManager, ApiUpdatesManager, PrvApiManager, $q, $timeout, _, Storage) {
    $scope.user = AppUsersManager.getSelf()
    $scope.temples = []
    $scope.dataLoaded = false
    $scope.emptyResults = true

    $scope.filter = {
      country: null,
      city: null,
      name: null
    }

    function loadData() {
      var params = {
        name: $scope.filter.name,
        country_id: $scope.filter.country.id,
        city_id: $scope.filter.city.id,
        limit: 50
      };

      PrvApiManager.getAction('temple/list', params).then(function (data) {
        $scope.dataLoaded = true
        $scope.temples = (data.items) ? data.items : []
        $scope.emptyResults = ($scope.temples.length) ? false : true
        $timeout(function () {
          $scope.$broadcast('temple_list_change')
        })
      }, function (error) {

      })
    }

    Storage.get('prv_country', 'prv_city').then(function (settings) {
      if (settings[0])
        $scope.filter.country = settings[0]
      if (settings[1])
        $scope.filter.city = settings[1]

    })

    $scope.$watchGroup(['filter.city', 'filter.name'], function () {
      loadData();
    })

    $scope.templeSelect = function (temple) {
      return $modalInstance.close(temple)
    }

    $scope.addTemple = function () {
      var scope = $rootScope.$new()
      scope.cityId = $scope.filter.city.id
      scope.countryId = $scope.filter.country.id

      $modal.open({
        templateUrl: templateUrl('prv_temple_add_modal'),
        controller: 'TempleAddModalController',
        scope: scope,
        windowClass: 'prv_md_simple_modal_window mobile_modal',
        backdrop: 'single'
      })

      $modalInstance.dismiss()
    }

    $scope.chooseCountry = function () {
      var modal = $modal.open({
        templateUrl: templateUrl('prv_country_select_modal'),
        controller: 'PrvCountrySelectModalController',
        windowClass: 'countries_modal_window mobile_modal',
        backdrop: 'single'
      })

      modal.result.then(function (country) {
        $scope.filter.country = country
        $scope.$broadcast('country_selected')
        $scope.chooseCity()
      })
    }

    $scope.chooseCity = function () {
      var scope = $rootScope.$new()
      scope.countryId = $scope.filter.country.id

      var modal = $modal.open({
        templateUrl: templateUrl('prv_city_select_modal'),
        controller: 'PrvCitySelectModalController',
        scope: scope,
        windowClass: 'countries_modal_window mobile_modal hidden_close',
        backdrop: 'static'
      })

      modal.result.then(function (city) {
        $scope.filter.city = city
        $scope.$broadcast('city_selected')
      }, function (reason) {
        if (reason == 'go_back') {
          $scope.chooseCountry();
        }
      })
    }
  })

  //alex
  .controller('TempleViewModalController', function ($scope, $modalInstance, $modal, $rootScope, MtpApiManager, AppUsersManager, AppChatsManager, ApiUpdatesManager, PrvApiManager, $q, $timeout, toaster, _, $location) {
    $scope.user = AppUsersManager.getSelf()
    $scope.temple = null;
    $scope.group = null;
    $scope.channel = null;
    $scope.dataLoaded = false
    $scope.slice = {limit: 1, limitDelta: 1}

    function confirmChatRequest(params, options) {
      options = options || {}
      var scope = $rootScope.$new()
      angular.extend(scope, params)

      var modal = $modal.open({
        templateUrl: templateUrl('prv_confirm_chat_modal'),
        scope: scope,
        windowClass: options.windowClass || 'confirm_modal_window'
      })

      return modal.result
    }

    //create new link in temple
    PrvApiManager.getAction('temple/view', {id: $scope.templeId}).then(function (data) {
      $scope.dataLoaded = true

      $scope.temple = data

      if ($scope.temple.chats != undefined && $scope.temple.chats.length) {
        angular.forEach($scope.temple.chats, function (chat) {
          if (chat.type == 0) {
            $scope.group = chat;
          }

          if (chat.type == 2) {
            $scope.channel = chat;
          }
        })
      }

      $timeout(function () {
        $scope.$broadcast('ui_height')
      })

    }, function (error) {

    });


    $scope.addToFavourite = function () {
      PrvApiManager.postAction('user/addTemple', {temple_id: $scope.temple.id}).then(function (data) {
        $scope.temple.is_my_temple = true;
      }, function (error) {
      });
    }

    $scope.removeFromFavourite = function () {
      PrvApiManager.postAction('user/removeTemple', {temple_id: $scope.temple.id}).then(function (data) {
        $scope.temple.is_my_temple = false;
      }, function (error) {
      });
    }

    $scope.goToGroup = function () {
      if ($scope.group) {
        //$rootScope.$broadcast('history_focus', {peerString: AppChatsManager.getChatString($scope.group.peer_id)})
        $location.url($scope.group.invitation_link.replace(/https:\/\/telegram.me\/joinchat\//gi, '/im?tgaddr=tg%3A%2F%2Fjoin%3Finvite%3D'));
      } else {
        confirmChatRequest({type: 'CHAT_GROUP'}).then(function () {
          PrvApiManager.postAction('chat/request', {
            type: 0,
            temple_id: $scope.temple.id,
            user_phone: $scope.user.phone
          }).then(function (data) {
            var toastData = toaster.pop({
              type: 'info',
              body: _('confirm_modal_chat_group_success'),
              bodyOutputType: 'trustedHtml',
              clickHandler: function () {
                toaster.clear(toastData)
              },
              showCloseButton: true
            })
          }, function (error) {
            var message = error.message || _('an_error_has_occurred');
            var toastData = toaster.pop({
              type: 'error',
              body: message,
              bodyOutputType: 'trustedHtml',
              clickHandler: function () {
                toaster.clear(toastData)
              },
              showCloseButton: true
            })
          });
        })
      }
    }

    $scope.goToChannel = function () {
      if ($scope.channel && $scope.channel.invitation_link) {
        $location.url($scope.channel.invitation_link.replace(/https:\/\/telegram.me\//gi, '/im?tgaddr=tg%3A%2F%2Fresolve%3Fdomain%3D'));

        //$rootScope.$broadcast('history_focus', {peerString: AppChatsManager.getChatString($scope.channel.peer_id)})
      } else {
        confirmChatRequest({type: 'CHAT_CHANNEL'}).then(function () {
          PrvApiManager.postAction('chat/request', {
            type: 2,
            temple_id: $scope.temple.id,
            user_phone: $scope.user.phone
          }).then(function () {
            var toastData = toaster.pop({
              type: 'info',
              body: _('confirm_modal_chat_channel_success'),
              bodyOutputType: 'trustedHtml',
              clickHandler: function () {
                toaster.clear(toastData)
              },
              showCloseButton: true
            })
          }, function (error) {
            var message = error.message || _('an_error_has_occurred');
            var toastData = toaster.pop({
              type: 'error',
              body: message,
              bodyOutputType: 'trustedHtml',
              clickHandler: function () {
                toaster.clear(toastData)
              },
              showCloseButton: true
            })
          });
        })
      }
    }
  })

  //alex
  .controller('UserTempleListModalController', function ($scope, $modalInstance, $modal, $rootScope, MtpApiManager, AppUsersManager, AppChatsManager, ApiUpdatesManager, PrvApiManager, $q, $timeout) {
    $scope.user = AppUsersManager.getSelf()
    $scope.temples = []
    $scope.dataLoaded = false
    $scope.emptyResults = true

    function loadData() {
      $scope.dataLoaded = false

      PrvApiManager.getAction('user/temples').then(function (data) {
        $scope.temples = (data.items) ? data.items : []
        $scope.dataLoaded = true
        $scope.emptyResults = ($scope.temples.length) ? false : true
        $timeout(function () {
          $scope.$broadcast('temple_list_change')
        })
      }, function (error) {

      })
    }

    $scope.templeSelect = function (templeId) {
      var scope = $rootScope.$new()
      scope.templeId = templeId

      var modal = $modal.open({
        templateUrl: templateUrl('prv_temple_view_modal'),
        controller: 'TempleViewModalController',
        scope: scope,
        windowClass: 'prv_md_simple_modal_window mobile_modal',
        backdrop: 'single'
      })

      modal.result.then(function () {
        loadData();
      }, function () {
        loadData();
      })
    }

    $scope.openTempleList = function (templeId) {
      var scope = $rootScope.$new()
      var modalTemple = $modal.open({
        templateUrl: templateUrl('prv_temple_list_modal'),
        controller: 'TempleListModalController',
        scope: scope,
        windowClass: 'prv_md_simple_modal_window mobile_modal',
        backdrop: 'single'
      })

      modalTemple.result.then(function (temple) {
        var scope = $rootScope.$new()
        scope.templeId = temple.id

        var modalTempleView = $modal.open({
          templateUrl: templateUrl('prv_temple_view_modal'),
          controller: 'TempleViewModalController',
          scope: scope,
          windowClass: 'prv_md_simple_modal_window mobile_modal',
          backdrop: 'single'
        })

        modalTempleView.result.then(function () {
          loadData();
        }, function () {
          loadData();
        })
      })
    }

    loadData()
  })

  .controller('TempleAddModalController', function ($scope, $modalInstance, $modal, $rootScope, MtpApiManager, AppUsersManager, AppChatsManager, ApiUpdatesManager, PrvApiManager, $q, $timeout, toaster, _) {
    $scope.user = AppUsersManager.getSelf()
    $scope.temple = {};

    $scope.form_files = []

    $scope.loading = false;

    $scope.add = function () {
      var formData = new FormData();

      angular.forEach($scope.form_files, function (file, key) {
        formData.append('file' + key, file);
      });

      formData.append("churchName", $scope.temple.name);
      formData.append("churchAddress", $scope.temple.address);

      if ($scope.countryId != undefined)
        formData.append("countryId", $scope.countryId);

      if ($scope.cityId != undefined)
        formData.append("cityId", $scope.cityId);

      $scope.loading = true;

      PrvApiManager.postAction('user/createTemple', formData, {
        transformRequest: angular.identity,
        headers: {'Content-Type': false}
      }).then(function () {
        $scope.loading = false;

        var toastData = toaster.pop({
          type: 'info',
          body: _('add_temple_success'),
          bodyOutputType: 'trustedHtml',
          clickHandler: function () {
            toaster.clear(toastData)
          },
          showCloseButton: true
        })

        $modalInstance.close()
      }, function (error) {
        var message = error.message || _('an_error_has_occurred');
        var toastData = toaster.pop({
          type: 'error',
          body: message,
          bodyOutputType: 'trustedHtml',
          clickHandler: function () {
            toaster.clear(toastData)
          },
          showCloseButton: true
        })
      });
    }

    $scope.selectFile = function () {
      var files = event.target.files;
      var file = files[0] || null;
      if(file !== null){
        if (!file.name.match(/\.(jpg|jpeg|png|gif)$/)) {
          var toastData = toaster.pop({
            type: 'error',
            body: _('unsupported_image_format_file'),
            bodyOutputType: 'trustedHtml',
            clickHandler: function () {
              toaster.clear(toastData)
            },
            showCloseButton: true
          })
          return
        }

        $scope.form_files.push(file);
      }
    }

    $scope.removeFile = function () {
      $scope.form_files.splice(this.$index, 1);
    }
  })

  .controller('PrvCitySelectModalController', function ($scope, $modalInstance, $rootScope, _, PrvApiManager) {
    $scope.search = {}
    $scope.slice = {limit: 20, limitDelta: 20}
    $scope.cities = []
    var cities = []
    $scope.dataLoaded = false
    var searchIndex = SearchIndexManager.createIndex()


    PrvApiManager.getAction('city/list', {country_id: $scope.countryId}).then(function (data) {
      cities = data.items
      for (var i = 0; i < cities.length; i++) {
        SearchIndexManager.indexObject(i, cities[i].name, searchIndex)
      }
      $scope.cities = cities
      $scope.dataLoaded = true
      updateList()
    }, function (error) {
      //@Todo
      /*ErrorService.show({
       error: {code: 403, type: 'PHONEBOOK_GET_CONTACTS_FAILED', originalError: error}
       })*/
    })

    function updateList() {
      var filtered = false
      var results = {}

      if (angular.isString($scope.search.query) && $scope.search.query.length) {
        filtered = true
        results = SearchIndexManager.search($scope.search.query, searchIndex)

        $scope.cities = []
        delete $scope.citiesEmpty
        for (var i = 0; i < cities.length; i++) {
          if (!filtered || results[i]) {
            $scope.cities.push(cities[i])
          }
        }
      } else {
        $scope.cities = cities
        $scope.citiesEmpty = !$scope.cities.length
      }

      $scope.slice.limit = 20
    }

    $scope.$watch('search.query', function (newValue) {
      if ($scope.dataLoaded) {
        updateList()
      }
    })

    $scope.citySelect = function (city) {
      return $modalInstance.close(city)
    }

    $scope.goBack = function () {
      return $modalInstance.dismiss('go_back')
    }
  })

  .controller('PrvCountrySelectModalController', function ($scope, $modalInstance, $rootScope, _, PrvApiManager) {
    $scope.search = {}
    $scope.slice = {limit: 20, limitDelta: 20}
    $scope.countries = []
    var countries = []
    var searchIndex = SearchIndexManager.createIndex()
    $scope.dataLoaded = false;


    PrvApiManager.getAction('country/list').then(function (data) {
      countries = data.items
      for (var i = 0; i < countries.length; i++) {
        SearchIndexManager.indexObject(i, countries[i].name, searchIndex)
      }
      $scope.countries = countries
      $scope.dataLoaded = true
      updateList()
    }, function (error) {
      //@Todo
      /*ErrorService.show({
       error: {code: 403, type: 'PHONEBOOK_GET_CONTACTS_FAILED', originalError: error}
       })*/
    })

    function updateList() {
      var filtered = false
      var results = {}

      if (angular.isString($scope.search.query) && $scope.search.query.length) {
        filtered = true
        results = SearchIndexManager.search($scope.search.query, searchIndex)

        $scope.countries = []
        delete $scope.countriesEmpty
        for (var i = 0; i < countries.length; i++) {
          if (!filtered || results[i]) {
            $scope.countries.push(countries[i])
          }
        }
      } else {
        $scope.countries = countries
        $scope.countriesEmpty = !$scope.countries.length
      }

      $scope.slice.limit = 20
    }

    $scope.$watch('search.query', function (newValue) {
      if ($scope.dataLoaded) {
        updateList()
      }
    })

    $scope.countrySelect = function (country) {
      return $modalInstance.close(country)
    }
  })

  .controller('PrvProfileSaintListModalController', function ($scope, $modalInstance, $rootScope, _, PrvApiManager, $timeout) {
    $scope.saints = []
    $scope.dataLoaded = false
    $scope.initialized = false
    $scope.emptyResults = true
    $scope.slice = {limit: 20, limitDelta: 20}

    $scope.filter = {
      name: ''
    }

    function loadData() {

      $scope.dataLoaded = false

      var params = {
        name: $scope.filter.name
      };
      PrvApiManager.getAction('profile/getSaints', params).then(function (data) {
        $scope.dataLoaded = true
        $scope.initialized = true
        $scope.saints = (data.items) ? data.items : []
        $scope.emptyResults = ($scope.saints.length) ? false : true
        $timeout(function () {
          $scope.$broadcast('saint_list_change')
        })
      })
    }

    $scope.$watch('filter.name', function (newVal) {
      if (newVal != '')
        loadData();
    })

    $scope.saintSelect = function (saint) {
      PrvApiManager.postAction('profile/save', {
        saint_id: saint.id
      }).then(function () {
        $modalInstance.close(saint)
      }, function (error) {
        $modalInstance.close()
      })
    }
  })

  .controller('PrvProfileProfessionModalController', function ($scope, $modalInstance, $rootScope, _, PrvApiManager, $timeout) {
    $scope.prv_profile = {}

    PrvApiManager.getAction('profile/view').then(function (data) {
      $scope.prv_profile = data
    })

    $scope.updateProfile = function () {
      $scope.prv_profile.updating = true
      PrvApiManager.postAction('profile/save', {
        profession: $scope.prv_profile.profession
      }).then(function () {
        $modalInstance.close($scope.prv_profile.profession)
      }, function (error) {
        $modalInstance.close()
      })['finally'](function () {
        delete $scope.profile.updating
      })
    }
  })

  .controller('PrvProfileBirthdayModalController', function ($scope, $modalInstance, $rootScope, _, PrvApiManager, $timeout, toaster) {
    var days = []
    var years = []

    for (var i = 1; i <= 31; i++) {
      days.push(i)
    }

    for (var i = 1917; i <= new Date().getFullYear(); i++) {
      years.push(i)
    }

    $scope.prv_profile = {}
    $scope.days = days
    $scope.months = ["january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"]
    $scope.years = years.reverse()

    $scope.dateSelect = {
      year: '',
      month: '',
      day: ''
    };


    PrvApiManager.getAction('profile/view').then(function (data) {
      $scope.prv_profile = data
    })

    $scope.updateProfile = function () {
      if ($scope.dateSelect.year == '' || $scope.dateSelect.month == '' || $scope.dateSelect.day == '') {
        var toastData = toaster.pop({
          type: 'error',
          body: _('profile_birthday_error_requiered'),
          bodyOutputType: 'trustedHtml',
          clickHandler: function () {
            toaster.clear(toastData)
          },
          showCloseButton: true
        })
        return;
      }

      $scope.prv_profile.updating = true

      var month = $scope.dateSelect.month <= 9 ? '0' + $scope.dateSelect.month : $scope.dateSelect.month;
      var day = $scope.dateSelect.day <= 9 ? '0' + $scope.dateSelect.day : $scope.dateSelect.day;

      $scope.prv_profile.birthday = $scope.dateSelect.year + '-' + month + '-' + day

      PrvApiManager.postAction('profile/save', {
        birthday: $scope.prv_profile.birthday
      }).then(function () {

        $modalInstance.close($scope.prv_profile.birthday)
      }, function (error) {
        $modalInstance.close()
      })['finally'](function () {
        delete $scope.profile.updating
      })
    }
  })

  .controller('PrvProfileEditModalController', function ($rootScope, $scope, $timeout, $modal, AppUsersManager, AppChatsManager, AppPhotosManager, MtpApiManager, Storage, NotificationsManager, MtpApiFileManager, PasswordManager, ApiUpdatesManager, ChangelogNotifyService, LayoutSwitchService, AppRuntimeManager, ErrorService, _, PrvApiManager) {
    //alex
    $scope.prv_profile = {}

    PrvApiManager.getAction('profile/view').then(function (data) {
      $scope.prv_profile = data
    }, function (error) {

    })

    $scope.changePrvBirthday = function () {
      var scope = $rootScope.$new()

      var modalBirthday = $modal.open({
        templateUrl: templateUrl('prv_profile_birthday_modal'),
        controller: 'PrvProfileBirthdayModalController',
        scope: scope,
        windowClass: 'prv_md_simple_modal_window mobile_modal',
        backdrop: 'single'
      })

      modalBirthday.result.then(function (birthday) {
        $scope.prv_profile.birthday = birthday;
      });
    }

    $scope.changePrvSaint = function () {
      var scope = $rootScope.$new()

      var modalSaintList = $modal.open({
        templateUrl: templateUrl('prv_profile_saint_list_modal'),
        controller: 'PrvProfileSaintListModalController',
        scope: scope,
        windowClass: 'prv_md_simple_modal_window mobile_modal',
        backdrop: 'single'
      })

      modalSaintList.result.then(function (saint) {
        $scope.prv_profile.saint = saint;
      });
    };

    $scope.changePrvProfession = function () {
      var modal = $modal.open({
        templateUrl: templateUrl('prv_profile_profession_modal'),
        controller: 'PrvProfileProfessionModalController',
        windowClass: 'md_simple_modal_window mobile_modal'
      })

      modal.result.then(function (profession) {
        $scope.prv_profile.profession = profession;
      })
    }
  })

  .controller('PrvJoinController', function ($scope, $location, $routeParams, MtpApiManager, ErrorService, ChangelogNotifyService, LayoutSwitchService, Storage) {
    Storage.set({'invite_temple': $routeParams.templeId})

    MtpApiManager.getUserID().then(function (id) {
      if (id) {
        $location.url('/im')
        return
      }
      if (location.protocol == 'http:' && !Config.Modes.http &&
        Config.App.domains.indexOf(location.hostname) != -1) {
        location.href = location.href.replace(/^http:/, 'https:')
        return
      }
      $location.url('/login')
    })

    ChangelogNotifyService.checkUpdate()
    LayoutSwitchService.start()
  })